<?php
/*
Plugin Name: No-IP
Plugin URI: http://wordpress.org/support/topic/37363
Description: Causes WordPress to NOT save the IP address from ANY commenter. WordPress will instead record all comment IP addresses as '127.0.0.1'
Author: WP-Forums
Author URI: http://wordpress.org/support/
Version: 0.1
*/

function zap_ip($ip) {
return '127.0.0.1';
}

// function zap_user_agent($agent) {
// return ' ';
// }

add_filter('pre_comment_user_ip', 'zap_ip');
// add_filter('pre_comment_user_agent', 'zap_user_agent');
?>
    <footer id="footer">
        <div class="inner">
            <h2>У партнерстві з</h2>
            <div id="footer-logo"><a href="http://www.ipaidabribe.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-footer2.png"></a></div>
            <ul>
                <?php
                if (is_active_sidebar('homepage-social-buttons')) :
                    dynamic_sidebar('homepage-social-buttons');
                endif;
                ?>
            </ul>            
        </div>
    </footer>
</div>
</html>
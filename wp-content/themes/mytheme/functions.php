<?php

function mytheme_setup() {
	register_nav_menu( 'primary', __( 'Navigation Menu', 'mytheme' ) );
	register_nav_menu( 'footer', __( 'Footer Menu', 'mytheme' ) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150, true );
    // add_image_size( 'homepage-leftsidebar', 92, 79, true ); 
    // add_image_size( 'homepage-slider', 1041, 534, true ); 
    // add_image_size( 'homepage-news', 355, 180, true ); 
    // add_image_size( 'homepage-multimedia', 183, 103, true ); 
}
add_action( 'after_setup_theme', 'mytheme_setup' );

// function add_class_to_menu_items($output, $args) {
// 	if( $args->theme_location == 'primary' )
//   		$output = preg_replace('/class="menu-item/', 'class="col span_1_5 menu-item', $output);
//   return $output;
// }
// add_filter('wp_nav_menu', 'add_class_to_menu_items', 10, 2);

// function add_div_to_menu_items($output, $args) {
// 	if( $args->theme_location == 'footer' )
//   		$output = preg_replace('/<a href/', '<div class="arrow-right"></div><a href', $output);
//   return $output;
// }
// add_filter('wp_nav_menu', 'add_div_to_menu_items', 10, 2);

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

if (!function_exists('iweb_reverse_comments')) {
    function iweb_reverse_comments($comments) {
        return array_reverse($comments);
    }   
}
add_filter ('comments_array', 'iweb_reverse_comments');

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function mytheme_widgets_init() {

/************* HEADER *********************/
    register_sidebar( array(
      'id'            => 'homepage-social-buttons',
      'name'          => __( 'Homepage - Social buttons', 'mytheme' ),
      'before_widget'  => '',                  
    ) );

    // register_sidebar( array(
    //   'id'            => 'homepage-newsletter',
    //   'name'          => __( 'Homepage - Newsletter', 'mytheme' ),
    //   'before_widget'  => '',                  
    // ) );

    register_sidebar( array(
      'id'            => 'homepage-intro',
      'name'          => __( 'Homepage - Intro', 'mytheme' ),
      'before_widget'  => '',                  
    ) );
 
    // register_sidebar( array(
    //   'id'            => 'footer-slogan',
    //   'name'          => __( 'Footer - Slogan', 'mytheme' ),
    //   'before_widget'  => '',                  
    // ) ); 

    // register_sidebar( array(
    //   'id'            => 'footer-contact',
    //   'name'          => __( 'Footer - Contact', 'mytheme' ),
    //   'before_widget'  => '',                  
    // ) );      

    // register_sidebar( array(
    //   'id'            => 'footer-copyright',
    //   'name'          => __( 'Footer - Copyright', 'mytheme' ),
    //   'before_widget'  => '',                  
    // ) );       

    // register_sidebar( array(
    //   'id'            => 'sidebar-blog',
    //   'name'          => __( 'Sidebar - Single Post', 'mytheme' ),
    //   'before_widget'  => '',                  
    // ) );  
}
add_action( 'widgets_init', 'mytheme_widgets_init' );

/************* CUSTOM WIDGETS *****************/


/************* SOCIAL LINKS *****************/

class SocialLinkWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Social Link','description=Social Link');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'class' => '', 'link' => '' ) );
			    $class = $instance['class'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('class'); ?>">CSS Class: </label><input class="widefat" id="<?php echo $this->get_field_id('class'); ?>" name="<?php echo $this->get_field_name('class'); ?>" type="text" value="<?php echo attribute_escape($class); ?>"/></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['class'] = $new_instance['class'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $class = empty($instance['class']) ? '' : apply_filters('widget_title', $instance['class']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

		    if (!empty($link))
		      echo "<li class='".$class."'><a href='".$link."' target='_blank'></a></li>";

		    echo $after_widget;
        }

}
register_widget( 'SocialLinkWidget' );

/************* HELPER FUNCTIONS *****************/

//Remove menu div container
function prefix_nav_menu_args($args = ''){
    $args['container'] = false;
    return $args;
}
add_filter('wp_nav_menu_args', 'prefix_nav_menu_args');

// function html_widget_title( $title ) {
// 	//HTML tag opening/closing brackets
// 	$title = str_replace( '[', '<', $title );
// 	$title = str_replace( ']', '/>', $title );

// 	return $title;
// }
// add_filter( 'widget_title', 'html_widget_title' );

// function html_widget_text( $text ) {
// 	//HTML tag opening/closing brackets
// 	$text = str_replace( '[', '<', $text );
// 	$text = str_replace( ']', '/>', $text );

// 	return $text;
// }
// add_filter( 'widget_text', 'html_widget_text' );

function mytheme_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
      $tag = 'div';
      $add_below = 'comment';
    } else {
      $tag = 'li';
      $add_below = 'div-comment';
    }
?>
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
<!--     <div class="comment-author vcard">
    <?php// if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
    <?php //printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
    </div> -->
<?php if ($comment->comment_approved == '0') : ?>
    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
    <br />
<?php endif; ?>

<!--     <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
      <?php
        /* translators: 1: date, 2: time */
        //printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
      ?>
    </div> -->
    <?php //echo str_pad($comment->comment_ID, 4, 0, STR_PAD_LEFT); ?>
    <div class="comment-meta"><?php echo $comment->comment_ID; ?></div>

    <?php comment_text() ?>

    <div class="reply">
    <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
<?php
        }